<?php
declare(strict_types=1);

namespace App\Command;

use App\Handler\Provider\Exception\XmlNotLoadedException;
use App\Handler\XmlUploadHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class XmlUploadCommand extends Command
{
    protected static $defaultName = 'xml-to-spreadsheet:upload';
    private const FILE_PATH_ARGUMENT = 'file-path';
    private const OWNER_EMAIL_ARGUMENT = 'owner-email';
    private XmlUploadHandlerInterface $handler;
    private LoggerInterface $logger;

    public function __construct(
        XmlUploadHandlerInterface $handler,
        LoggerInterface $logger
    ) {
        $this->handler = $handler;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument(
                self::FILE_PATH_ARGUMENT,
                InputArgument::REQUIRED,
                'Path to the file',
            )
            ->addArgument(
                self::OWNER_EMAIL_ARGUMENT,
                InputArgument::REQUIRED,
                'Document owner e-mail'
            )
            ->setDescription('Upload xml file to google sheets')
            ->setHelp('This command allows you to upload xml document as spreadsheet to google sheets.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $start_upload_message = "Starting upload: {$input->getArgument(self::FILE_PATH_ARGUMENT)}";
        $owner_message = "Owner: {$input->getArgument(self::OWNER_EMAIL_ARGUMENT)}";
        $io = new SymfonyStyle($input, $output);
        $io->title('XML to Spreadsheet uploader.');
        $this->logger->info($start_upload_message);
        $this->logger->info($owner_message);
        $io->info($start_upload_message);
        $io->info($owner_message);

        try {
            $this->handler->handle(
                $input->getArgument(self::FILE_PATH_ARGUMENT),
                $input->getArgument(self::OWNER_EMAIL_ARGUMENT)
            );
        } catch (XmlNotLoadedException $e) {
            $this->logger->error($e->getMessage());
            $io->error("Failed to load file: {$e->getMessage()}");
            return 1;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $io->error("Upload failed: {$e->getMessage()}");
            return 1;
        }

        $success_message = 'Upload success.';
        $io->success($success_message);
        $this->logger->info($success_message);
        return 0;
    }
}


