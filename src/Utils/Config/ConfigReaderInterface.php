<?php
declare(strict_types=1);

namespace App\Utils\Config;

interface ConfigReaderInterface
{
    public function load(): void;

    public function getAutoConfigPath(): string;
}
