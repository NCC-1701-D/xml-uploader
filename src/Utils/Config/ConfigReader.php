<?php
declare(strict_types=1);

namespace App\Utils\Config;

use Symfony\Component\Dotenv\Dotenv;

final class ConfigReader implements ConfigReaderInterface
{
    private Dotenv $env;

    public function __construct(Dotenv $env)
    {
        $this->env = $env;
    }

    public function load(): void
    {
        $this->env->load(__DIR__ . '/../../../.env');
    }

    public function getAutoConfigPath(): string
    {
        return $_ENV['AUTO_CONFIG_PATH'];
    }
}
