<?php
declare(strict_types=1);

namespace App\Utils\Logger\Factory;

use Psr\Log\LoggerInterface;

interface LoggerFactoryInterface
{
    public function createLogger(): LoggerInterface;
}
