<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use App\Handler\Api\ApiClient;
use App\Command\XmlUploadCommand;
use App\Handler\Api\Builder\ClientBuilder;
use App\Handler\Api\Builder\ClientDirector;
use App\Handler\Api\Builder\Exception\ClientConfigurationException;
use App\Handler\Mapper\XmlArrayToSpreadsheetArrayMapper;
use App\Handler\Provider\XmlFileProvider;
use App\Handler\Converter\XmlToArrayConverter;
use App\Handler\XmlUploadHandler;
use App\Utils\Config\ConfigReader;
use App\Utils\Logger\Factory\LoggerFactory;
use Google\Service\Drive;
use Google\Service\Sheets;
use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Dotenv;

$config_reader = new ConfigReader(new Dotenv());
$config_reader->load();
$logger = (new LoggerFactory())->createLogger();
$client = null;

try {
    $client = (new ClientDirector(new ClientBuilder()))->build($config_reader->getAutoConfigPath());
} catch (ClientConfigurationException $e) {
    $logger->error($e->getMessage());
    echo "An error occurred while starting the application: {$e->getMessage()}";
    return 1;
}

$application = new Application();

$application->add(
    new XmlUploadCommand(
        new XmlUploadHandler(
            new XmlFileProvider(),
            new XmlArrayToSpreadsheetArrayMapper(),
            new XmlToArrayConverter(),
            new ApiClient(new Sheets($client), new Drive($client))
        ),
        $logger
    )
);

$application->run();
