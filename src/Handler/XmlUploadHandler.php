<?php
declare(strict_types=1);

namespace App\Handler;

use App\Handler\Api\ApiClientInterface;
use App\Handler\Api\ValueObject\SpreadsheetData;
use App\Handler\Provider\XMLFileProviderInterface;
use App\Handler\Mapper\XmlArrayToSpreadsheetArrayMapperInterface;
use App\Handler\Converter\XmlToArrayConverterInterface;

final class XmlUploadHandler implements XmlUploadHandlerInterface
{
    private XMLFileProviderInterface $file_content_provider;
    private XmlArrayToSpreadsheetArrayMapperInterface $xml_array_to_spreadsheet_array_mapper;
    private XmlToArrayConverterInterface $converter;
    private ApiClientInterface $api_client;

    public function __construct(
        XMLFileProviderInterface $file_content_provider,
        XmlArrayToSpreadsheetArrayMapperInterface $xml_array_to_spreadsheet_array_mapper,
        XmlToArrayConverterInterface $converter,
        ApiClientInterface $api_client
    ) {
        $this->file_content_provider = $file_content_provider;
        $this->xml_array_to_spreadsheet_array_mapper = $xml_array_to_spreadsheet_array_mapper;
        $this->converter = $converter;
        $this->api_client = $api_client;
    }

    public function handle(string $path, string $owner_email): void
    {
        $xml = $this->file_content_provider->getXml($path);
        $xml_array = $this->converter->convert($xml);
        $spreadsheet_values = $this->xml_array_to_spreadsheet_array_mapper->map($xml_array);
        $this->api_client->uploadSpreadsheet(
            new SpreadsheetData(
                $xml->getName(),
                $spreadsheet_values,
                $owner_email
            )
        );
    }
}
