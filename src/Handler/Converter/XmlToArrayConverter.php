<?php
declare(strict_types=1);

namespace App\Handler\Converter;

final class XmlToArrayConverter implements XmlToArrayConverterInterface
{
    public function convert(\SimpleXMLElement $xml): array
    {
        return json_decode(json_encode((array)$xml), true);
    }
}
