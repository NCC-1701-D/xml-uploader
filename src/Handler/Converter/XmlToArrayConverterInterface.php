<?php
declare(strict_types=1);

namespace App\Handler\Converter;

interface XmlToArrayConverterInterface
{
    public function convert(\SimpleXMLElement $xml): array;
}
