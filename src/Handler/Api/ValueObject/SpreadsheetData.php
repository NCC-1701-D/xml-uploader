<?php
declare(strict_types=1);

namespace App\Handler\Api\ValueObject;

final class SpreadsheetData
{
    private string $name;
    private array $values;
    private string $owner_email;

    public function __construct(string $name, array $values, string $owner_email)
    {
        $this->name = $name;
        $this->values = $values;
        $this->owner_email = $owner_email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function getOwnerEmail(): string
    {
        return $this->owner_email;
    }
}
