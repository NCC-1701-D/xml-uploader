<?php
declare(strict_types=1);

namespace App\Handler\Api;

use App\Handler\Api\ValueObject\SpreadsheetData;

interface ApiClientInterface
{
    public function uploadSpreadsheet(SpreadsheetData $spreadsheet_data): void;
}
