<?php
declare(strict_types=1);

namespace App\Handler\Api\Builder;

use Google\Client;

interface ClientDirectorInterface
{
    public function build(string $auto_config_path): Client;
}
