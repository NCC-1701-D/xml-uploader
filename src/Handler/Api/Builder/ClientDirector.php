<?php
declare(strict_types=1);

namespace App\Handler\Api\Builder;

use Google\Client;
use Google\Service\Drive;
use Google\Service\Sheets;

final class ClientDirector implements ClientDirectorInterface
{
    private ClientBuilderInterface $client_builder;

    public function __construct(ClientBuilderInterface $client_builder)
    {
        $this->client_builder = $client_builder;
    }

    public function build(string $auto_config_path): Client
    {
        return $this->client_builder
            ->createClient()
            ->setApplicationName('Xml Uploader')
            ->setScopes([Sheets::SPREADSHEETS, Drive::DRIVE])
            ->setAccessType('online')
            ->setAuthConfig($auto_config_path)
            ->getClient();
    }
}
