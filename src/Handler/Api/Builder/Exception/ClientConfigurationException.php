<?php
declare(strict_types=1);

namespace App\Handler\Api\Builder\Exception;

final class ClientConfigurationException extends \Exception
{
}
