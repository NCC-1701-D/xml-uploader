<?php
declare(strict_types=1);

namespace App\Handler\Api\Builder;

use Google\Client;

interface ClientBuilderInterface
{
    public function createClient(): self;

    public function setApplicationName(string $application_name): self;

    /** @param string[] $scopes */
    public function setScopes(array $scopes): self;

    public function setAuthConfig(string $auto_config_path): self;

    public function setAccessType(string $access_type): self;

    public function getClient(): Client;
}
