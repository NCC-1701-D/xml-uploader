<?php
declare(strict_types=1);

namespace App\Handler\Api\Builder;

use App\Handler\Api\Builder\Exception\ClientConfigurationException;
use Google\Client;
use Google\Exception;

final class ClientBuilder implements ClientBuilderInterface
{
    private Client $client;

    public function createClient(): ClientBuilderInterface
    {
        $this->client = new Client();
        return $this;
    }

    public function setApplicationName(string $application_name): ClientBuilderInterface
    {
        $this->client->setApplicationName($application_name);
        return $this;
    }

    public function setScopes(array $scopes): ClientBuilderInterface
    {
        $this->client->setScopes($scopes);
        return $this;
    }

    /**
     * @throws ClientConfigurationException
     */
    public function setAuthConfig(string $auto_config_path): ClientBuilderInterface
    {
        try {
            $this->client->setAuthConfig($auto_config_path);
        } catch (Exception $e) {
            throw new ClientConfigurationException($e->getMessage(), $e->getCode(), $e);
        }
        return $this;
    }

    public function setAccessType(string $access_type): ClientBuilderInterface
    {
        $this->client->setAccessType($access_type);
        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
