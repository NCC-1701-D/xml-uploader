<?php
declare(strict_types=1);

namespace App\Handler\Api;

use App\Handler\Api\ValueObject\SpreadsheetData;
use Google\Service\Drive;
use Google\Service\Drive\Permission;
use Google\Service\Sheets;
use Google\Service\Sheets\Spreadsheet;
use Google\Service\Sheets\ValueRange;

final class ApiClient implements ApiClientInterface
{
    private Sheets $spreadsheet_service;
    private Drive $drive_service;

    public function __construct(Sheets $spreadsheet_service, Drive $drive_service)
    {
        $this->spreadsheet_service = $spreadsheet_service;
        $this->drive_service = $drive_service;
    }

    public function uploadSpreadsheet(SpreadsheetData $spreadsheet_data): void
    {
        $spreadsheet = $this->createSpreadsheet($spreadsheet_data->getName());
        $this->updateSpreadsheet($spreadsheet, $spreadsheet_data->getValues());
        $this->transferOwnership($spreadsheet, $spreadsheet_data->getOwnerEmail());
    }

    private function createSpreadsheet(string $name): Spreadsheet
    {
        $spreadsheet = new Spreadsheet([
            'properties' => [
                'title' => $name
            ]
        ]);

        return $this->spreadsheet_service->spreadsheets->create($spreadsheet, [
            'fields' => 'spreadsheetId'
        ]);
    }

    private function updateSpreadsheet(Spreadsheet $spreadsheet, array $values): void
    {
        $this->spreadsheet_service->spreadsheets_values->update(
            $spreadsheet->spreadsheetId,
            'Sheet1',
            new ValueRange(
                [
                    'values' => $values
                ]
            ),
            [
                'valueInputOption' => 'RAW'
            ]
        );
    }

    private function transferOwnership(Spreadsheet $spreadsheet, string $owner_email): void
    {
        $this->drive_service->permissions->create(
            $spreadsheet->spreadsheetId,
            new Permission(
                [
                    'type' => 'user',
                    'role' => 'owner',
                    'emailAddress' => $owner_email
                ]
            ),
            [
                'fields' => 'id',
                'transferOwnership' => 'true'
            ]
        );
    }
}
