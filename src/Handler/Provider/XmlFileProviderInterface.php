<?php
declare(strict_types=1);

namespace App\Handler\Provider;

interface XmlFileProviderInterface
{
    public function getXml(string $path): \SimpleXMLElement;
}
