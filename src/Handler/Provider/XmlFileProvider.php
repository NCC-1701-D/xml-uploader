<?php
declare(strict_types=1);

namespace App\Handler\Provider;

use App\Handler\Provider\Exception\XmlNotLoadedException;

final class XmlFileProvider implements XmlFileProviderInterface
{
    /**
     * @throws XmlNotLoadedException
     */
    public function getXml(string $path): \SimpleXMLElement
    {
        error_clear_last();
        $content = @file_get_contents($path);
        if ($content === false) {
            $error = $this->getLastErrorOrDefault("Check that the file exists and can be read.");
            throw new XmlNotLoadedException("Xml file '$path' was not loaded. $error");
        }

        return new \SimpleXMLElement($content);
    }

    private function getLastErrorOrDefault(string $default): string
    {
        $e = error_get_last();
        if (isset($e) && isset($e['message']) && $e['message'] != "") {
            return $e['message'];
        }

        return $default;
    }
}
