<?php
declare(strict_types=1);

namespace App\Handler\Provider\Exception;

final class XmlNotLoadedException extends \Exception
{
}
