<?php
declare(strict_types=1);

namespace App\Handler;

interface XmlUploadHandlerInterface
{
    public function handle(string $path, string $owner_email): void;
}
