<?php
declare(strict_types=1);

namespace App\Handler\Mapper;

final class XmlArrayToSpreadsheetArrayMapper implements XmlArrayToSpreadsheetArrayMapperInterface
{
    public function map(array $xml_array): array
    {
        $values = array_values($xml_array);

        $final_values = [];
        $final_values[] = array_keys($values[0][0]);

        foreach ($values[0] as $item) {
            $row = [];
            foreach ($item as $key => $value) {
                $row[] = is_array($value) ? implode(',', $value) : $value;
            }
            $final_values[] = $row;
        }

        return $final_values;
    }
}
