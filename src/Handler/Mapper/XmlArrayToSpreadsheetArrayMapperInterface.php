<?php
declare(strict_types=1);

namespace App\Handler\Mapper;

interface XmlArrayToSpreadsheetArrayMapperInterface
{
    public function map(array $xml_array): array;
}
