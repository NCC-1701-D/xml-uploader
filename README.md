# Xml Uploader

## Prerequisites
1. [Docker](https://docs.docker.com/engine/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)
2. Project at Google Cloud Platform with:
    - [enabled Google Sheets API and Google Drive API](https://developers.google.com/workspace/guides/create-project)
    - [service account and downloaded JSON key](https://cloud.google.com/docs/authentication/getting-started)


## Setting up
1. Before you build the application, you need to copy the downloaded **JSON key** to the directory with the **Dockerfile**. 
The copied file must be named **credentials.json**. This directory contains a sample **credentials.json.dist** file.

1. Set up environment variables:

        cp .env.dist .env
        
2. Run the docker container:

        docker-compose up -d --build
        
3. Log in to the php-fpm container:

        docker exec -it php-fpm /bin/bash
        
4. Install dependencies(in container):

        composer install
        
 
## How to use(in the container)

### Uploading xml file from an external source:
You will use the command:
                    
    php src/Application.php xml-to-spreadsheet:upload

with arguments:

    file-path             Path to the file
    owner-email           Document owner e-mail

which gives you:

    php src/Application.php xml-to-spreadsheet:upload ftp://username:password@some.site.io/file.xml owner.email@gmail.com

of course, instead of **owner.email@gmail.com** you must use the e-mail address of Google Sheets user, who will be the owner of the uploaded document.

#### Known issues:
Sometimes you have to try few times because they can be some connection issues.


### Uploading xml file from a local source:

    php src/Application.php xml-to-spreadsheet:upload /path/to/file.xml owner.email@gmail.com

The file path can be relative or absolute.

The file you want to upload must be in the container.

See [docker cp](https://docs.docker.com/engine/reference/commandline/cp/)

See [Use volumes](https://docs.docker.com/storage/volumes/)

You can also copy the file to the project directory.

## To run the tests(in container):

    php vendor/phpunit/phpunit/phpunit tests/

## Logs
The log files are available in the **/var/www/html/var/logs** directory in the container or in the **var/logs** directory in the project directory.

## TODO
- refactor the api module
- fix connection issues
- increase test coverage


