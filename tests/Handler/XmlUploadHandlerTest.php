<?php
declare(strict_types=1);

namespace App\Tests\Handler;

use App\Handler\Api\ApiClientInterface;
use App\Handler\Converter\XmlToArrayConverter;
use App\Handler\Mapper\XmlArrayToSpreadsheetArrayMapper;
use App\Handler\Provider\XmlFileProviderInterface;
use App\Handler\XmlUploadHandler;
use PHPUnit\Framework\TestCase;

final class XmlUploadHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testHandler(string $xml): void
    {
        $file_content_provider = $this->createMock(XmlFileProviderInterface::class);
        $file_content_provider
            ->method('getXml')
            ->willReturn(new \SimpleXMLElement($xml));
        $api_client = $this->getMockBuilder(ApiClientInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $api_client
            ->expects($this->once())
            ->method('uploadSpreadsheet');

        $handler = new XmlUploadHandler(
            $file_content_provider,
            new XmlArrayToSpreadsheetArrayMapper(),
            new XmlToArrayConverter(),
            $api_client
        );

        $handler->handle('fake_path', 'fake_email');
    }

    public function getTestData(): array
    {
        return [
            [
                '<?xml version="1.0" encoding="utf-8"?>
                    <list>
                        <item>
                            <id>340</id>
                            <details>details 1</details>
                        </item>
                        <item>
                            <id>342</id>
                            <details>details 2</details>
                        </item>
                    </list>'
            ]
        ];
    }
}
