<?php
declare(strict_types=1);

namespace App\Tests\Handler\Mapper;

use App\Handler\Mapper\XmlArrayToSpreadsheetArrayMapper;
use PHPUnit\Framework\TestCase;

final class XmlArrayToSpreadsheetArrayMapperTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testMap(array $xml_array, array $expected): void
    {
        $mapper = new XmlArrayToSpreadsheetArrayMapper();
        $result = $mapper->map($xml_array);

        $this->assertEquals($expected, $result);
    }

    public function getTestData(): array
    {
        return [
            [
                [
                    'item' => [
                        [
                            'id' => '340',
                            'details' => 'details 1'
                        ],
                        [
                            'id' => '342',
                            'details' => 'details 2'
                        ]
                    ]
                ],
                [
                    ["id", "details"],
                    ["340", "details 1"],
                    ["342", "details 2"]
                ]
            ]
        ];
    }
}
