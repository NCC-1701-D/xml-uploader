<?php
declare(strict_types=1);

namespace App\Tests\Handler\Converter;

use App\Handler\Converter\XmlToArrayConverter;
use PHPUnit\Framework\TestCase;

final class XmlToArrayConverterTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testConvert(string $xml, array $expected): void
    {
        $converter = new XmlToArrayConverter();
        $converted_xml = $converter->convert(new \SimpleXMLElement($xml));

        $this->assertEquals($expected, $converted_xml);
    }

    public function getTestData(): array
    {
        return [
            [
                '<?xml version="1.0" encoding="utf-8"?>
                    <list>
                        <item>
                            <id>340</id>
                            <details>details 1</details>
                        </item>
                        <item>
                            <id>342</id>
                            <details>details 2</details>
                        </item>
                    </list>'
                ,
                [
                    'item' => [
                        [
                            'id' => '340',
                            'details' => 'details 1'
                        ],
                        [
                            'id' => '342',
                            'details' => 'details 2'
                        ]
                    ]
                ]
            ]
        ];
    }
}
